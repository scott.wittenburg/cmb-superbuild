message(STATUS "Configuring modulebuild+ACE3P package")
set_property(GLOBAL PROPERTY "${SUPERBUILD_PACKAGE_MODE}_REQUIRED_PROJECTS" "smtkace3p;cmb;smtk")
set_property(GLOBAL PROPERTY "${SUPERBUILD_PACKAGE_MODE}_EXCLUDE_PROJECTS" "")

include(SuperbuildVersionMacros)
superbuild_set_version_variables(smtkace3p "1.0.0" "smtkace3p-version.cmake" "version.txt")

set(package_extra_projects
  smtkace3p
  openssl
  cumulus
)
list(APPEND superbuild_extra_package_projects "${package_extra_projects}")

list(APPEND smtk_plugin_omit
  smtkDelaunayPlugin
  smtkMeshPlugin
  smtkMeshSessionPlugin
  smtkOscillatorSessionPlugin
  smtkPVMeshExtPlugin
  smtkPolygonSessionPlugin)
set_property(GLOBAL PROPERTY smtk_plugins_omit ${smtk_plugin_omit})
#set_property(GLOBAL APPEND PROPERTY cmb_extra_python_modules smtksimulationace3p)
set_property(GLOBAL APPEND PROPERTY cmb_extra_plugins ace3p-extensions)
set_property(GLOBAL APPEND PROPERTY cmb_extra_dependencies smtkace3p)

# Configure Options
set(ENABLE_smtkace3p ON)
set(ENABLE_cmb ON)
set(ENABLE_cmbworkflows OFF)
set(ENABLE_smtkresourcemanagerstate OFF)

set(ace3p_target_system "NERSC/Cori" CACHE STRING "Set the target system for remote visualization (default: NERSC/Cori)")
set_property(CACHE ace3p_target_system PROPERTY STRINGS "NERSC/Cori;Custom")

message(STATUS "ACE3P targeting: ${ace3p_target_system}")
if (ace3p_target_system STREQUAL "NERSC/Cori")
  message(STATUS "Using modified version of ParaView 5.9 (for/slac-pv59)")
  superbuild_set_source_selection(paraview for-slac)
endif ()

set_property(GLOBAL APPEND PROPERTY smtk_extra_package_cmake_arguments
  -DSMTK_ENABLE_OPERATION_THREADS:BOOL=OFF
  -DSMTK_ENABLE_PROJECT_UI:BOOL=OFF)

set_property(GLOBAL APPEND PROPERTY cmb_extra_package_cmake_arguments
  -Dcmb_enable_multiservers:BOOL=ON
  -Dcmb_enable_objectpicking:BOOL=ON)

# Turn on OpenSSL
set(_superbuild_enable_openssl ON)
set(ENABLE_openssl ON)

if (DEVELOPER_MODE_smtk)
  message(FATAL_ERROR "Cannot build in ace3p packaging mode if DEVELOPER_MODE_smtk is ON")
endif ()

include(CMBBundleMacros)
cmb_generate_package_suffix(${SUPERBUILD_PACKAGE_MODE})
cmb_generate_package_bundle(${SUPERBUILD_PACKAGE_MODE}
  PACKAGE_NAME "modelbuilder-${SUPERBUILD_PACKAGE_MODE}"
  DESCRIPTION "CMB + ACE3P"
  PACKAGE_VERSION smtkace3p
  HAS_WORKFLOWS
  EXCLUDE_VERSION
  )
