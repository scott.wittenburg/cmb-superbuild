message(STATUS "Configuring modelbuilder-2d package")
set_property(GLOBAL PROPERTY cmb2d_REQUIRED_PROJECTS "cmb;smtk")
set_property(GLOBAL PROPERTY cmb2d_EXCLUDE_PROJECTS "")

include(SuperbuildVersionMacros)
superbuild_set_version_variables(smtkcmb2dextensions "1.0.0" "smtkcmb2dextensions-version.cmake" "version.txt")

set(cmb2d_extra_projects
  smtk
  smtkcmb2dextensions
)
list(APPEND superbuild_extra_package_projects "${cmb2d_extra_projects}")

list(APPEND smtk_plugin_omit
  smtkOscillatorSessionPlugin)
set_property(GLOBAL PROPERTY smtk_plugins_omit ${smtk_plugin_omit})
set_property(GLOBAL APPEND PROPERTY cmb_extra_python_modules smtksimulationcmb2d)
set_property(GLOBAL APPEND PROPERTY cmb_extra_plugins cmb2d-extensions XMSMeshOperation)
set_property(GLOBAL APPEND PROPERTY cmb_extra_dependencies smtkcmb2dextensions smtkxmsmesher)

# Configure options
set(ENABLE_cmb ON)
set(ENABLE_smtk ON)
set(ENABLE_smtkxmsmesher ON)
set(ENABLE_smtkcmb2dextensions ON)

set(ENABLE_cmbworkflows OFF)
set(ENABLE_opencascadesession OFF)
set(ENABLE_smtkresourcemanagerstate OFF)

# Note: async opertions are not compatible with the python export operation
set_property(GLOBAL APPEND PROPERTY smtk_extra_package_cmake_arguments
  -DSMTK_ENABLE_OPERATION_THREADS:BOOL=OFF
  -DSMTK_ENABLE_PROJECT_UI:BOOL=OFF)

# Use the legacy object picking mode
set_property(GLOBAL APPEND PROPERTY cmb_extra_package_cmake_arguments
  -Dcmb_enable_objectpicking:BOOL=ON)

include(CMBBundleMacros)
cmb_generate_package_suffix(cmb2d)
cmb_generate_package_bundle(cmb2d
  PACKAGE_NAME "modelbuilder-2d"
  DESCRIPTION "CMB + 2D Simulation Workflows"
  PACKAGE_VERSION smtkcmb2dextensions
  HAS_WORKFLOWS
  EXCLUDE_VERSION
)
