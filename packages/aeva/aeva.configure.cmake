message(STATUS "Configuring aeva package")

include(SuperbuildVersionMacros)
superbuild_configure_project_version(aeva)

set(aeva_extra_projects
  aeva
  aevasession
  aevaexampledata
)

list(APPEND projects_with_plugins aevasession)
set(aevasession_plugin_files
  "${aevasession_plugin_dir}/aeva.session.xml")

# Project Configuration
set_property(GLOBAL PROPERTY aeva_REQUIRED_PROJECTS "aeva;aevasession;aevaexampledata;cmb;smtk")
set_property(GLOBAL PROPERTY aeva_EXCLUDE_PROJECTS "")
list(APPEND superbuild_extra_package_projects "${aeva_extra_projects}")

# Omit unused plugins
list(APPEND smtk_plugin_omit
  smtkDelaunayPlugin
  # Omit this so aeva's configuration is uncontested:
  smtkDefaultConfigurationPlugin
  # Omit these to avoid unused mesh selections
  smtkMeshPlugin
  smtkPVMeshExtPlugin
  # Unused sesssion plugins
  smtkMeshSessionPlugin
  smtkOscillatorSessionPlugin
  smtkPolygonSessionPlugin
  smtkVTKSessionPlugin)
set_property(GLOBAL PROPERTY smtk_plugins_omit ${smtk_plugin_omit})

# Exclude all of the CMB plugins
set_property(GLOBAL PROPERTY cmb_plugin_omit "<all>")

# Configure options
set(ENABLE_aeva ON)
set(ENABLE_cmb OFF)
set(ENABLE_pythonmeshio ON)
set(ENABLE_numpy ON)
set(ENABLE_gdal OFF)

# Needed for ITK's usage of HDF5.
set(hdf5_build_cpp ON)

# TODO: auto generate the bundle scripts
#include(CMBBundleMacros)
#cmb_generate_bundle(aeva)
include(CMBBundleMacros)
cmb_generate_package_bundle(aeva
  CPACK_NAME "AEVA"
  PACKAGE_NAME "aevaCMB"
  APPLICATIONS "aevaCMB"
  PACKAGE_VERSION aeva
  HAS_EXAMPLES
  )
