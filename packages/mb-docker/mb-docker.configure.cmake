message(STATUS "Configuring modelbuilder+docker package")
set_property(GLOBAL PROPERTY modelbuilder_REQUIRED_PROJECTS "cmb;smtk;cmbworkflows")
set_property(GLOBAL PROPERTY modelbuilder_EXCLUDE_PROJECTS "")

set(package_extra_projects
  openssl
  pythondocker
)
list(APPEND superbuild_extra_package_projects "${package_extra_projects}")

# SMTK Configuration
list(APPEND smtk_plugin_omit
  smtkDelaunayPlugin
  smtkMeshPlugin
  smtkMeshSessionPlugin
  smtkOscillatorSessionPlugin
  smtkPVMeshExtPlugin
  smtkPolygonSessionPlugin)
set_property(GLOBAL PROPERTY smtk_plugins_omit ${smtk_plugin_omit})
set_property(GLOBAL APPEND PROPERTY cmb_extra_package_cmake_arguments "-Dcmb_enable_pythonshell:BOOL=ON")
# Currently cannot support python operations with threads
set_property(GLOBAL APPEND PROPERTY smtk_extra_package_cmake_arguments
  -DSMTK_ENABLE_OPERATION_THREADS:BOOL=OFF
  -DSMTK_ENABLE_PROJECT_UI:BOOL=OFF)

# Configure options
set(ENABLE_cmbworkflows ON)
set(ENABLE_cmb ON)
set(ENABLE_opencascadesession ON)
set(ENABLE_pythondocker ON)

# Turn on OpenSSL for superbuild, it is needed for the pythondocker package
set(_superbuild_enable_openssl ON)

include(CMBBundleMacros)
cmb_generate_package_bundle(${SUPERBUILD_PACKAGE_MODE}
  PACKAGE_NAME "modelbuilder-docker"
  DESCRIPTION "CMB + Docker"
  HAS_EXAMPLES
  HAS_WORKFLOWS
)
