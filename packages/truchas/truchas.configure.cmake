message(STATUS "Configuring modulebuild+truchas package")
set_property(GLOBAL PROPERTY truchas_REQUIRED_PROJECTS "modelbuilder;smtktruchasextensions;cmb;smtk")
set_property(GLOBAL PROPERTY truchas_EXCLUDE_PROJECTS "")

include(SuperbuildVersionMacros)
superbuild_set_version_variables(smtktruchasextensions "1.0.0" "smtktruchasextensions-version.cmake" "version.txt")

set(truchas_extra_projects
  smtk
  smtktruchasextensions
)
list(APPEND superbuild_extra_package_projects "${truchas_extra_projects}")

list(APPEND smtk_plugin_omit
  smtkDelaunayPlugin
  smtkMeshPlugin
  smtkMeshSessionPlugin
  smtkOscillatorSessionPlugin
  smtkPVMeshExtPlugin
  smtkPolygonSessionPlugin)
set_property(GLOBAL PROPERTY smtk_plugins_omit ${smtk_plugin_omit})
set_property(GLOBAL APPEND PROPERTY cmb_extra_python_modules smtksimulationtruchas)
set_property(GLOBAL APPEND PROPERTY cmb_extra_plugins truchas-extensions)
set_property(GLOBAL APPEND PROPERTY cmb_extra_dependencies smtktruchasextensions)

# Configure options
set(ENABLE_smtktruchasextensions ON)
set(ENABLE_cmbworkflows OFF)
set(ENABLE_cmb ON)
set(ENABLE_smtkresourcemanagerstate OFF)

set_property(GLOBAL APPEND PROPERTY smtk_extra_package_cmake_arguments
  -DSMTK_ENABLE_OPERATION_THREADS:BOOL=OFF
  -DSMTK_ENABLE_PROJECT_UI:BOOL=OFF)

if (DEVELOPER_MODE_smtk)
  message(FATAL_ERROR "Cannot build in truchas packaging mode if DEVELOPER_MODE_smtk is ON")
endif ()

include(CMBBundleMacros)
cmb_generate_package_suffix(truchas)
cmb_generate_package_bundle(truchas
  PACKAGE_NAME "modelbuilder-truchas"
  DESCRIPTION "CMB + Truchas Extentions"
  PACKAGE_VERSION smtktruchasextensions
  HAS_EXAMPLES
  HAS_WORKFLOWS
  EXCLUDE_VERSION
  )
