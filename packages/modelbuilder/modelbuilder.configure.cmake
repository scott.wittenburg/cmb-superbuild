message(STATUS "Configuring modelbuilder package")
set_property(GLOBAL PROPERTY modelbuilder_REQUIRED_PROJECTS "cmb;smtk;cmbworkflows")
set_property(GLOBAL PROPERTY modelbuilder_EXCLUDE_PROJECTS "")

# This variable holds all of the versions of modelbuilder that can be built
set(modelbuilder_versions
  # NEW_VERSION
  "22.04.0"
  )

set(all_modelbuilder_versions
  "<development>"
  ${modelbuilder_versions})
set(modelbuilder_RELEASE "<development>"
  CACHE STRING "The version of CMB to build")
set_property(CACHE modelbuilder_RELEASE
  PROPERTY
    STRINGS "${all_modelbuilder_versions}")

if (modelbuilder_RELEASE STREQUAL "<development>")
  # Nothing special needed.
elseif (NOT modelbuilder_RELEASE IN_LIST modelbuilder_versions)
  message(FATAL_ERROR
    "Unsupported modelbuilder version ${modelbuilder_RELEASE}.")
#elseif (modelbuilder_RELEASE STREQUAL "NEW_VERSION")
#  set(cmb_SOURCE_SELECTION "NEW_VERSION")
#  set(smtk_SOURCE_SELECTION "NEW_VERSION")
#  set(paraview_SOURCE_SELECTION "for-vNEW_VERSION")
elseif (modelbuilder_RELEASE STREQUAL "22.04.0")
  set(cmb_SOURCE_SELECTION "22.04")
  set(smtk_SOURCE_SELECTION "22.04")
  set(paraview_SOURCE_SELECTION "for-v22.04")
else ()
  message(FATAL_ERROR
    "Unhandled modelbuilder version ${modelbuilder_RELEASE}.")
endif ()

# Configure options
set(ENABLE_cmbworkflows ON)
set(ENABLE_cmb ON)
set(ENABLE_opencascadesession OFF)

# Omit these SMTK plugins
list(APPEND smtk_plugin_omit
  smtkDelaunayPlugin
  smtkMeshSessionPlugin
  smtkOscillatorSessionPlugin
  smtkPolygonSessionPlugin)
set_property(GLOBAL PROPERTY smtk_plugins_omit ${smtk_plugin_omit})

include(CMBBundleMacros)
cmb_generate_package_bundle(modelbuilder
  HAS_EXAMPLES
  HAS_WORKFLOWS)
