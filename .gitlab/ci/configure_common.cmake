set(CTEST_USE_LAUNCHERS "ON" CACHE STRING "")

# Enable SMTK testing.
set(TEST_smtk "ON" CACHE BOOL "")
set(smtk_FETCH_LFS "ON" CACHE BOOL "")

# Disable CMB testing.
set(TEST_cmb "OFF" CACHE BOOL "")

# Suppress various project outputs. It is too large for the log to follow.
set(SUPPRESS_itk_OUTPUT "ON" CACHE BOOL "")
set(SUPPRESS_paraview_OUTPUT "ON" CACHE BOOL "")
set(SUPPRESS_python3_OUTPUT "ON" CACHE BOOL "")
set(SUPPRESS_pythonply_OUTPUT "ON" CACHE BOOL "")
set(SUPPRESS_qt5_OUTPUT "ON" CACHE BOOL "")

# Hide noise from the CI logs. Also ensures logs are on CDash.
set(SUPPRESS_aeva_OUTPUT "ON" CACHE BOOL "")
set(SUPPRESS_aevasession_OUTPUT "ON" CACHE BOOL "")
set(SUPPRESS_eigen_OUTPUT "ON" CACHE BOOL "")
set(SUPPRESS_ffi_OUTPUT "ON" CACHE BOOL "")
set(SUPPRESS_itkvtkglue_OUTPUT "ON" CACHE BOOL "")
set(SUPPRESS_libarchive_OUTPUT "ON" CACHE BOOL "")
set(SUPPRESS_libxml2_OUTPUT "ON" CACHE BOOL "")
set(SUPPRESS_moab_OUTPUT "ON" CACHE BOOL "")
set(SUPPRESS_netgen_OUTPUT "ON" CACHE BOOL "")
set(SUPPRESS_nlohmannjson_OUTPUT "ON" CACHE BOOL "")
set(SUPPRESS_opencascadesession_OUTPUT "ON" CACHE BOOL "")
set(SUPPRESS_pegtl_OUTPUT "ON" CACHE BOOL "")
set(SUPPRESS_pythoncertifi_OUTPUT "ON" CACHE BOOL "")
set(SUPPRESS_pythonidna_OUTPUT "ON" CACHE BOOL "")
set(SUPPRESS_pythonmeshio_OUTPUT "ON" CACHE BOOL "")
set(SUPPRESS_pythonurllib3_OUTPUT "ON" CACHE BOOL "")
set(SUPPRESS_pythonwheel_OUTPUT "ON" CACHE BOOL "")
set(SUPPRESS_smtk_OUTPUT "ON" CACHE BOOL "")
set(SUPPRESS_smtkresourcemanagerstate_OUTPUT "ON" CACHE BOOL "")
set(SUPPRESS_sqlite_OUTPUT "ON" CACHE BOOL "")
set(SUPPRESS_tbb_OUTPUT "ON" CACHE BOOL "")

# Hide some "errors" from CTest. Remove when Python2 is no longer needed.
set(SUPPRESS_pythonsetuptools_OUTPUT "ON" CACHE BOOL "")

# Default to Release builds.
if ("$ENV{CMAKE_BUILD_TYPE}" STREQUAL "")
  set(CMAKE_BUILD_TYPE "Release" CACHE STRING "")
else ()
  set(CMAKE_BUILD_TYPE "$ENV{CMAKE_BUILD_TYPE}" CACHE STRING "")
endif ()

# Build binaries that will run on older architectures
if ("$ENV{CMAKE_CONFIGURATION}" MATCHES "centos7")
  set(CMAKE_C_FLAGS "-march=core2 -mno-avx512f" CACHE STRING "")
  set(CMAKE_CXX_FLAGS "-march=core2 -mno-avx512f" CACHE STRING "")
endif ()

# Build binaries that will run on older architectures
if ("$ENV{CMAKE_CONFIGURATION}" MATCHES "macos_x86_64")
  set(CMAKE_C_FLAGS "-march=core2 -mno-bmi -mno-bmi2" CACHE STRING "")
  set(CMAKE_CXX_FLAGS "-march=core2 -mno-bmi -mno-bmi2" CACHE STRING "")
endif ()

if (NOT "$ENV{CI_JOB_NAME}" MATCHES "windows")
  include("${CMAKE_CURRENT_LIST_DIR}/configure_sccache.cmake")
endif ()

# Configure the package to build
if ("$ENV{CMAKE_CONFIGURATION}" MATCHES "docker")
  set(SUPERBUILD_PACKAGE_MODE "mb-docker" CACHE STRING "")
elseif ("$ENV{CMAKE_CONFIGURATION}" MATCHES "modelbuilder$")
  # Capture the commit message for tagged releases.
  if ("$ENV{CI_COMMIT_TITLE}" MATCHES "^modelbuilder: add release v\(.*\)$")
    set(modelbuilder_RELEASE "${CMAKE_MATCH_1}"
      CACHE STRING "")
  endif ()

  set(SUPERBUILD_PACKAGE_MODE "modelbuilder" CACHE STRING "")
elseif ("$ENV{CMAKE_CONFIGURATION}" MATCHES "ace3p")
  set(SUPERBUILD_PACKAGE_MODE "ace3p" CACHE STRING "")
elseif ("$ENV{CMAKE_CONFIGURATION}" MATCHES "aeva")
  set(SUPERBUILD_PACKAGE_MODE "aeva" CACHE STRING "")
elseif ("$ENV{CMAKE_CONFIGURATION}" MATCHES "truchas")
  set(SUPERBUILD_PACKAGE_MODE "truchas" CACHE STRING "")
elseif ("$ENV{CMAKE_CONFIGURATION}" MATCHES "cmb2d")
  set(SUPERBUILD_PACKAGE_MODE "cmb2d" CACHE STRING "")
else ()
  set(SUPERBUILD_PACKAGE_MODE "<none>" CACHE STRING "")
  set(ENABLE_cmb "OFF" CACHE BOOL "")
  set(ENABLE_smtk "ON" CACHE BOOL "")
  set(ENABLE_smtkresourcemanagerstate "OFF" CACHE BOOL "")

  if ("$ENV{CI_JOB_NAME}" MATCHES "centos7")
    # matplotlib is not necessary, but this is as good a place as any to test it
    set(ENABLE_matplotlib "ON" CACHE BOOL "")
  else ()
    set(ENABLE_matplotlib "OFF" CACHE BOOL "")
  endif ()

  if ("$ENV{CMAKE_CONFIGURATION}" MATCHES "smtk")
    set(ENABLE_paraview "ON" CACHE BOOL "")
  elseif ("$ENV{CMAKE_CONFIGURATION}" MATCHES "vtk")
    set(ENABLE_paraview "OFF" CACHE BOOL "")
    set(ENABLE_vtkonly "ON" CACHE BOOL "")
  else ()
    message(FATAL_ERROR "Unrecognized configuration name")
  endif ()
endif ()
