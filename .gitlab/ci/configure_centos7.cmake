set(ENABLE_python3 ON CACHE BOOL "")

# Qt 5.12 has been working so far. Don't rock the boat.
set(qt5_SOURCE_SELECTION "5.12" CACHE STRING "")

include("${CMAKE_CURRENT_LIST_DIR}/configure_common.cmake")
