
set(_cmb_cmake_dir "${CMAKE_CURRENT_LIST_DIR}")
include(SuperbuildPackageMacros)

# TODO: Add a format string
#       %platformID-%platformVersion-%YY%MM%DD
function (cmb_generate_package_suffix name)
  string(TIMESTAMP datestamp "%y%m%d")
  if (NOT PLATFORM_STRING)
    if (APPLE)
      set(PLATFORM_STRING "macos-${CMAKE_SYSTEM_PROCESSOR}-${CMAKE_OSX_DEPLOYMENT_TARGET}" CACHE STRING "")
    elseif (UNIX)
      # TODO Grab names from /etc/os-release
      if (EXISTS "/etc/os-release")
        execute_process(
          COMMAND grep ^ID= /etc/os-release
          OUTPUT_VARIABLE platform
          RESULT_VARIABLE res
          ERROR_VARIABLE err
          OUTPUT_STRIP_TRAILING_WHITESPACE)
        string(REGEX REPLACE "ID=|\"" "" platform "${platform}" )
        execute_process(
          COMMAND grep ^VERSION_ID= /etc/os-release
          OUTPUT_VARIABLE platform_version
          RESULT_VARIABLE res
          ERROR_VARIABLE err
          OUTPUT_STRIP_TRAILING_WHITESPACE)
        string(REGEX REPLACE "VERSION_ID=|\"|\\." "" platform_version "${platform_version}" )
        set(PLATFORM_STRING "${platform}${platform_version}" CACHE STRING "" FORCE)
      else ()
        set(PLATFORM_STRING "Linux-64bit" CACHE STRING "" FORCE)
      endif ()
    elseif (WIN32)
      set(PLATFORM_STRING "windows" CACHE STRING "")
    else ()
      message(FATAL_ERROR "unsupported platform")
    endif ()
  endif ()
  set("${name}_PACKAGE_SUFFIX" "${PLATFORM_STRING}-${datestamp}" CACHE STRING "" FORCE)
endfunction ()

function (cmb_generate_package_bundle name)
  set(flag_args NO_DOC HAS_WORKFLOWS HAS_EXAMPLES EXCLUDE_VERSION)
  set(value_args SUFFIX DESCRIPTION CPACK_NAME PACKAGE_VERSION PACKAGE_NAME)
  set(multi_args APPLICATIONS)
  cmake_parse_arguments(bundle
    "${flag_args}"
    "${value_args}"
    "${multi_args}"
    ${ARGN})

  # Setup the extra arguments to pass to the CMake bundler
  set(cmb_configuration_variables)

  # This package does not include documentation
  if (NOT bundle_NO_DOC)
    list(APPEND cmb_configuration_variables cmb_doc_dir)
    if (APPLE)
      list(APPEND cmb_configuration_variables cmb_doc_base_dir)
    endif ()
  endif ()
  set_property(GLOBAL PROPERTY "${name}_PACKAGE_HAS_DOC" $<NOT bundle_HAS_DOC>)

  # This package has workflows
  if (bundle_HAS_WORKFLOWS)
    list(APPEND cmb_configuration_variables cmb_workflow_dir)
  endif ()
  set_property(GLOBAL PROPERTY "${name}_PACKAGE_HAS_WORKFLOWS" ${bundle_HAS_WORKFLOWS})

  # This package has examples
  if (bundle_HAS_EXAMPLES)
    list(APPEND cmb_configuration_variables cmb_example_dir)
  endif ()
  set_property(GLOBAL PROPERTY "${name}_PACKAGE_HAS_EXAMPLES" ${bundle_HAS_EXAMPLES})

  set(cmb_exclude_version FALSE)
  list(APPEND cmb_configuration_variables cmb_exclude_version)
  # Exclude the versionn from the package name
  if (bundle_EXCLUDE_VERSION)
    set(cmb_exclude_version TRUE)
  endif ()

  list(APPEND cmb_configuration_variables plugin_dir)

  if (NOT bundle_DESCRIPTION)
    set(bundle_DESCRIPTION "CMB ${name} application")
  endif ()
  set_property(GLOBAL PROPERTY "${name}_PACKAGE_DESCRIPTION" ${bundle_DESCRIPTION})

  if (NOT bundle_PACKAGE_NAME)
    set(bundle_PACKAGE_NAME "${name}")
  endif ()
  set_property(GLOBAL PROPERTY "${name}_PACKAGE_NAME" ${bundle_PACKAGE_NAME})

  if (NOT bundle_CPACK_NAME)
    string(TOUPPER "${bundle_PACKAGE_NAME}" bundle_CPACK_NAME)
  endif ()
  set_property(GLOBAL PROPERTY "${name}_CPACK_NAME" ${bundle_CPACK_NAME})

  if (NOT bundle_APPLICATIONS)
    set(bundle_APPLICATIONS "modelbuilder")
  endif ()
  set_property(GLOBAL PROPERTY "${name}_PACKAGE_APPLICATION" ${bundle_APPLICATIONS})

  if (NOT bundle_PACKAGE_VERSION)
    set(bundle_PACKAGE_VERSION "cmb")
  endif ()

  if (APPLE)
    set(cmb_doc_dir "share/cmb/doc")
    set(cmb_doc_base_dir "doc")
    set(cmb_workflow_dir "Contents/Resources/workflows")
    set(cmb_example_dir "examples")
    set(plugin_dir "lib")
    set(cmb_platform_bundle "cmb.bundle.apple")
  elseif (UNIX)
    set(cmb_doc_dir "share/cmb/doc")
    set(cmb_workflow_dir "share/cmb/workflows")
    set(cmb_example_dir "share/examples")
    set(plugin_dir "lib")
    set(cmb_platform_bundle "cmb.bundle.unix")
  elseif (WIN32)
    set(cmb_doc_dir "doc")
    set(cmb_workflow_dir "share/cmb/workflows")
    set(cmb_example_dir "examples")
    set(plugin_dir "bin")
    set(cmb_platform_bundle "cmb.bundle.windows")
  else ()
    message(FATAL_ERROR "unsupported platform")
  endif ()

  set("${name}_PACKAGE_SUFFIX" ${bundle_SUFFIX})
  superbuild_package_suffix(${name}_PACKAGE_SUFFIX)
  set(bundle_SUFFIX "${${name}_PACKAGE_SUFFIX}")

  set(cmb_platform_configuration_variables)
  foreach (variable IN LISTS cmb_configuration_variables)
    string(APPEND cmb_platform_configuration_variables "set(\"${variable}\" \"${${variable}}\")\n")
  endforeach ()
  configure_file("${_cmb_cmake_dir}/package.bundle.cmake.in"
                 "${CMAKE_BINARY_DIR}/cpack/${name}/cmake/${name}.bundle.cmake"
                 @ONLY)
  list(PREPEND CMAKE_MODULE_PATH "${CMAKE_BINARY_DIR}/cpack/${name}/cmake")
  set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} PARENT_SCOPE)
endfunction()
