include (SuperbuildTestingMacros)

set(modelbuilder_extract_root "${CMAKE_BINARY_DIR}/Testing/Temporary")

#[==[.md
Test that extracts modelbuilder package file

```
add_package_extract_test(generator)
```
This test calls the superbuild_add_extract_test to extract the modelbuilder
package file.

#]==]
function (add_package_extract_test name generator)
  # Get the extract directory
  set(extract_dir "${modelbuilder_extract_root}/${name}-${generator}/test-extracted")

  if (${name}_PACKAGE_FILE_NAME)
    set(glob_prefix "${${name}_PACKAGE_FILE_NAME}")
  else ()
    get_property(package_name GLOBAL PROPERTY "${name}_PACKAGE_NAME")
    if (package_name)
      set(glob_prefix "${package_name}-*")
    else ()
      message(WARNING "Property ${name}_PACKAGE_NAME was not set, assuming \"modelbuilder\" for the extract test")
      set(glob_prefix "modelbuilder-*")
    endif ()
    if (${name}_PACKAGE_SUFFIX AND NOT ${name}_PACKAGE_SUFFIX STREQUAL "<default>")
      set(glob_prefix "${glob_prefix}${${name}_PACKAGE_SUFFIX}")
    endif ()
  endif ()

  superbuild_add_extract_test(${name} "${glob_prefix}" "${generator}" "${extract_dir}" ${ARGN})
endfunction ()

#[==[.md
Tests that extracted modelbuilder executable can import smtk python modules.

```
add_package_import_test(generator)
```
This test runs modelbuilder and checks if it can import a minimal smtk operation,
which requires the python environment to be configured correctly.

  * The test command runs a cmake script that runs the modelbuilder executable.
  * Variables passed to the cmake script specify the paths to the executable,
    the xml script, and other needed info.
  * The xml script invokes the modelbuilder "Import Python Operation..." menu
    item and selects a python file "minimum_operation.py" in the scripts
    subfolder).
  * If the import operation is successful, a small text file is generated in
    the test directory.

#]==]
function (add_package_import_test name generator)
  set(extract_dir "${modelbuilder_extract_root}/${name}-${generator}/test-extracted")
  set(this_dir "${CMAKE_SOURCE_DIR}/cmake")
  set(test_dir "${CMAKE_BINARY_DIR}/Testing/Temporary")

  get_property(exe_name GLOBAL PROPERTY "${name}_PACKAGE_APPLICATION")
  if (NOT exe_name)
    message(WARNING "Property ${name}_PACKAGE_APPLICATION was not set, assuming \"modelbuilder\" for the import test")
    set(exe_name "modelbuilder")
  endif ()

  set(exe_path "bin/${exe_name}")
  if (APPLE)
    set(exe_path "${exe_name}.app/Contents/MacOS/${exe_name}")
  endif ()

  add_test(
    NAME "import-${name}-${generator}"
    COMMAND
      "${CMAKE_COMMAND}"
      "-DEXECUTABLE=${extract_dir}/${exe_path}"
      "-DEXPECTED_FILE=${test_dir}/minimum_operation.txt"
      "-DDATA_DIR=${this_dir}/scripts"
      "-DTEST_DIR=${test_dir}"
      "-DTEST_SCRIPT=${this_dir}/scripts/import_minimum_operation.xml"
      "-P" "${this_dir}/import_pythonop.cmake"
    WORKING_DIRECTORY "${test_dir}"
    )

  set_tests_properties("import-${name}-${generator}"
    PROPERTIES
      ${ARGN}
    )
endfunction ()
