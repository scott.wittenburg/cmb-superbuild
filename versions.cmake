option(SUPERBUILD_SHALLOW_CLONES "Perform shallow clones rather than full clones" ON)
mark_as_advanced(SUPERBUILD_SHALLOW_CLONES)

superbuild_set_revision(smtkace3p
  GIT_REPOSITORY  "https://gitlab.kitware.com/cmb/plugins/ace3p-extensions.git"
  GIT_TAG         "origin/master"
  GIT_SHALLOW     "${SUPERBUILD_SHALLOW_CLONES}")

# While the goal is to be consistent with a SHA on ParaView's main fork, CMB
# frequently uses a custom fork of ParaView in order to maintain functionality
# while updating the connections between the two projects. When this happens, it
# is critical that the forked ParaView also push ParaView's tags to the fork,
# since this is how ParaView determines its version (and, resultantly, how it
# names its libraries).
#
# To push ParaView's tags:
#   $ cd <path-to-sb-build-tree>/superbuild/paraview/src
#   $ git remote add kitware https://gitlab.kitware.com/paraview/paraview.git
#   $ git fetch --tags kitware
#   $ git push --tags origin

superbuild_set_selectable_source(paraview
  SELECT git PROMOTE DEFAULT
    GIT_REPOSITORY  "https://gitlab.kitware.com/paraview/paraview.git"
    # XXX: When updating, use the date from the commit as shown in `git log`
    # and a short description of why the hash was updated. DO NOT BLINDLY
    # COPY THE COMMIT MESSAGE OF THE SHA. EXPLAIN WHY SMTK/CMB/AEVA NEED
    # THE NEW PARAVIEW VERSION.
    #
    # What: master @ Fri May 6 19:53:36 2022
    # Why:  Enable using ParaView resource in modelbuilder
    GIT_TAG         "b60ebc9e586c5d16a5104434006140fd28eb6cd1"
  SELECT master CUSTOMIZABLE
    GIT_REPOSITORY  "https://gitlab.kitware.com/paraview/paraview.git"
    GIT_TAG         "origin/master"
    GIT_SHALLOW     "${SUPERBUILD_SHALLOW_CLONES}"
  SELECT for-slac CUSTOMIZABLE PROMOTE
    GIT_REPOSITORY  "https://gitlab.kitware.com/cmb/paraview.git"
    # for-slac-v5.9 is a ParaView v5.9 with backports required for remote-vis
    # and features used in the ace3p-extensions plugin
    GIT_TAG "for/slac-pv5.9"
  #SELECT for-vNEW_VERSION CUSTOMIZABLE
  #  GIT_REPOSITORY  "https://gitlab.kitware.com/cmb/paraview.git"
  #  GIT_TAG         "COPY_HASH_FROM_git_SELECTION"
  SELECT for-v22.04 CUSTOMIZABLE
    GIT_REPOSITORY  "https://gitlab.kitware.com/cmb/paraview.git"
    GIT_TAG         "e656b1803011aaa010647e7ca7f40ef28e53f2c2"
  SELECT source CUSTOMIZABLE
    SOURCE_DIR "source-paraview")

superbuild_set_selectable_source(vtkonly
  # VTK v9.0.2
  SELECT release PROMOTE DEFAULT
    URL     "https://www.vtk.org/files/release/9.0/VTK-9.0.2.tar.gz"
    URL_MD5 "1a2d280cd4377d551c3d013209e9d9ed"
  SELECT git CUSTOMIZABLE
    GIT_REPOSITORY  "https://gitlab.kitware.com/vtk/vtk.git"
    GIT_TAG         "origin/master"
    GIT_SHALLOW     "${SUPERBUILD_SHALLOW_CLONES}"
  SELECT source CUSTOMIZABLE
    SOURCE_DIR "source-vtkonly")

superbuild_set_selectable_source(cmb
  #SELECT NEW_VERSION
  #  GIT_REPOSITORY  "https://gitlab.kitware.com/cmb/cmb.git"
  #  GIT_TAG         "vNEW_VERSION"
  SELECT 22.04
    GIT_REPOSITORY  "https://gitlab.kitware.com/cmb/cmb.git"
    GIT_TAG         "v22.04.0"
  SELECT git CUSTOMIZABLE DEFAULT
    GIT_REPOSITORY  "https://gitlab.kitware.com/cmb/cmb.git"
    GIT_TAG         "origin/master"
    GIT_SHALLOW     "${SUPERBUILD_SHALLOW_CLONES}"
  SELECT source CUSTOMIZABLE
    SOURCE_DIR "source-cmb")

superbuild_set_selectable_source(smtk
  SELECT 22.05
    GIT_REPOSITORY  "https://gitlab.kitware.com/cmb/smtk.git"
    GIT_TAG         "v22.05.0"
  SELECT 22.04
    GIT_REPOSITORY  "https://gitlab.kitware.com/cmb/smtk.git"
    GIT_TAG         "v22.04.0"
  SELECT 22.02
    GIT_REPOSITORY  "https://gitlab.kitware.com/cmb/smtk.git"
    GIT_TAG         "v22.02.0"
  SELECT 21.12
    GIT_REPOSITORY  "https://gitlab.kitware.com/cmb/smtk.git"
    GIT_TAG         "v21.12.0"
  SELECT 21.11
    GIT_REPOSITORY  "https://gitlab.kitware.com/cmb/smtk.git"
    GIT_TAG         "v21.11.0"
  SELECT 21.10
    GIT_REPOSITORY  "https://gitlab.kitware.com/cmb/smtk.git"
    GIT_TAG         "v21.10.0"
  SELECT 21.09
    GIT_REPOSITORY  "https://gitlab.kitware.com/cmb/smtk.git"
    GIT_TAG         "v21.09.0"
  SELECT 21.07
    GIT_REPOSITORY  "https://gitlab.kitware.com/cmb/smtk.git"
    GIT_TAG         "v21.07.0"
  SELECT 21.05.0
    GIT_REPOSITORY  "https://gitlab.kitware.com/cmb/smtk.git"
    GIT_TAG         "v21.05.0"
  SELECT 21.04.0
    GIT_REPOSITORY  "https://gitlab.kitware.com/cmb/smtk.git"
    GIT_TAG         "v21.04.0"
  SELECT 3.3.0
    GIT_REPOSITORY  "https://gitlab.kitware.com/cmb/smtk.git"
    GIT_TAG         "v3.3.0"
  SELECT git CUSTOMIZABLE DEFAULT
    GIT_REPOSITORY  "https://gitlab.kitware.com/cmb/smtk.git"
    GIT_TAG         "origin/master"
    GIT_SHALLOW     "${SUPERBUILD_SHALLOW_CLONES}"
  SELECT source CUSTOMIZABLE
    SOURCE_DIR "source-smtk")

superbuild_set_revision(cmbworkflows
  GIT_REPOSITORY  "https://gitlab.kitware.com/cmb/simulation-workflows.git"
  GIT_TAG         "origin/master"
  GIT_SHALLOW     "${SUPERBUILD_SHALLOW_CLONES}")

superbuild_set_revision(libarchive
  URL     "https://www.paraview.org/files/dependencies/libarchive-3.6.0.tar.xz"
  URL_MD5 93f96acdb9e7277278edb154e5d76e49)

superbuild_set_revision(vxl
  GIT_REPOSITORY  "https://gitlab.kitware.com/third-party/vxl.git"
  GIT_TAG         origin/for/cmb
  GIT_SHALLOW     "ON")

superbuild_set_revision(itk
  GIT_REPOSITORY  "https://github.com/InsightSoftwareConsortium/ITK.git"
  # Fix compilation error with newer libstdc++ (missing <limits> include)
  # Current version: 5.3 (see `projects/itk.cmake`)
  GIT_TAG         15fb98a54a430d5fa4df024503733559bff23564
  # Cannot checkout shallow copy of a hash
  # GIT_SHALLOW     "ON"
)

superbuild_set_revision(itkvtkglue
  GIT_REPOSITORY  "https://gitlab.kitware.com/aeva/itkvtkglue.git"
  GIT_TAG         origin/for-aeva)

# Use opencv from Thu Oct 6 13:40:33 2016 +0000
superbuild_set_revision(opencv
  # https://github.com/opencv/opencv.git
  URL     "https://www.computationalmodelbuilder.org/files/dependencies/opencv-dd379ec9fddc1a1886766cf85844a6e18d38c4f1.tar.bz2"
  URL_MD5 19bbd14ed1bd741beccd6d19e444552f)

superbuild_set_revision(gdal
  # https://github.com/judajake/gdal-svn.git
  URL     "https://www.computationalmodelbuilder.org/files/dependencies/gdal-98353693d6f1d607954220b2f8b040375e3d1744.tar.bz2"
  URL_MD5 5aa285dcc856f98ce44020ae1ae192cb)

superbuild_set_revision(eigen
  # https://github.com/eigenteam/eigen-git-mirror/releases/tag/3.3.7
  URL "https://www.computationalmodelbuilder.org/files/dependencies/eigen-git-mirror-3.3.7.tar.gz"
  URL_MD5 77a2c934eaf35943c43ee600a83b72df)

superbuild_set_revision(moab
  # https://bitbucket.org/fathomteam/moab.git
  URL     "https://www.computationalmodelbuilder.org/files/dependencies/moab-6425a480ffe8e08b96453618fc5530e08e68ae8a.tar.bz2"
  URL_MD5 5946a405cddce972d54044fca6c87b11)

superbuild_set_revision(triangle
  # https://github.com/robertmaynard/triangle.git
  URL     "https://www.paraview.org/files/dependencies/triangle-4c20820448cdfa27f968cfd7cb33ea5b9426ad91.tar.bz2"
  URL_MD5 9a016bc90f1cdff441c75ceb53741b11)

superbuild_set_revision(pythonmeshio
  URL     "https://www.computationalmodelbuilder.org/files/dependencies/meshio-4.3.11.tar.gz"
  URL_MD5 56593e77540413ceaccaecccd6b55585)

superbuild_set_revision(pythondiskcache
  URL      "https://www.computationalmodelbuilder.org/files/dependencies/diskcache-3.1.0.tar.gz"
  URL_HASH SHA256=96cd1be1240257167a090794cce45db02ecf39d20b7a062580299b42107690ac)

superbuild_set_revision(pythongirderclient
  URL      "https://www.computationalmodelbuilder.org/files/dependencies/girder-client-2.4.0.tar.gz"
  URL_HASH SHA256=1a9c882b8bce2e8233f572a4df9565c678e5614993e6606cdff04251532cddcb)

superbuild_set_revision(pythonrequests
  URL     "https://www.computationalmodelbuilder.org/files/dependencies/requests-2.26.0.tar.gz"
  URL_MD5 8c745949ad3e9ae83d9927fed213db8a)

superbuild_set_revision(pythonrequeststoolbelt
  URL      "https://www.computationalmodelbuilder.org/files/dependencies/requests-toolbelt-0.8.0.tar.gz"
  URL_HASH SHA256=f6a531936c6fa4c6cfce1b9c10d5c4f498d16528d2a54a22ca00011205a187b5)

superbuild_set_revision(pythoncharsetnormalizer
  URL      "https://www.computationalmodelbuilder.org/files/dependencies/charset-normalizer-2.0.6.tar.gz"
  URL_HASH SHA256=5ec46d183433dcbd0ab716f2d7f29d8dee50505b3fdb40c6b985c7c4f5a3591f)

superbuild_set_revision(pythonidna
  URL      "https://www.computationalmodelbuilder.org/files/dependencies/idna-3.2.tar.gz"
  URL_HASH SHA256=467fbad99067910785144ce333826c71fb0e63a425657295239737f7ecd125f3)

superbuild_set_revision(pythonpysocks
  URL      "https://www.computationalmodelbuilder.org/files/dependencies/PySocks-1.7.1.tar.gz"
  URL_HASH SHA256=3f8804571ebe159c380ac6de37643bb4685970655d3bba243530d6558b799aa0)

superbuild_set_revision(pythonurllib3
  URL      "https://www.computationalmodelbuilder.org/files/dependencies/urllib3-1.26.7.tar.gz"
  URL_HASH SHA256=4987c65554f7a2dbf30c18fd48778ef124af6fab771a377103da0585e2336ece)

superbuild_set_revision(pythoncertifi
  URL      "https://www.computationalmodelbuilder.org/files/dependencies/certifi-2021.10.8.tar.gz"
  URL_HASH SHA256=78884e7c1d4b00ce3cea67b44566851c4343c120abd683433ce934a68ea58872)

superbuild_set_revision(pythondocker
  URL      "https://www.computationalmodelbuilder.org/files/dependencies/docker-5.0.0.tar.gz"
  URL_MD5 9cc5156a2ff6458a8f52114b9bbc0d7e)

superbuild_set_revision(pythonwebsocketclient
  URL      "https://www.computationalmodelbuilder.org/files/dependencies/websocket-client-1.1.0.tar.gz"
  URL_MD5 251ff2f9063dae8d4fca5e7e700d2b7b)

superbuild_set_revision(ftgl
  # https://github.com/ulrichard/ftgl.git
  URL     "https://www.paraview.org/files/dependencies/ftgl-dfd7c9f0dee7f0059d5784f3a71118ae5c0afff4.tar.bz2"
  URL_MD5 16e54c7391f449c942f3f12378db238f)

superbuild_set_revision(occt
  # https://git.dev.opencascade.org/gitweb/?p=occt.git pick the tag you want, and download a snapshot.
  # current: 7.4.0p1
  # Extract, delete docs, tests, and sample data, and recompress as .tar.bz2
  URL     "https://www.computationalmodelbuilder.org/files/dependencies/occt-7.4.0p1-stripped.tar.bz2"
  URL_MD5 24b95c5d55558ba087b9565f30c67718)

superbuild_set_revision(protobuf
  GIT_REPOSITORY "https://github.com/protocolbuffers/protobuf"
  GIT_TAG         v3.8.0
  SOURCE_SUBDIR   cmake
  GIT_SHALLOW     "ON")

superbuild_set_revision(cmbusersguide
  URL     "https://media.readthedocs.org/pdf/cmb/master/cmb.pdf")

superbuild_set_revision(smtkusersguide
  URL     "https://media.readthedocs.org/pdf/smtk/latest/smtk.pdf")

superbuild_set_revision(aevaexampledata
  URL     "https://data.kitware.com/api/v1/file/6194eb2d2fa25629b9dc75b5/download/aeva-example.zip")

# Use json from Wed Mar 20 21:03:30 2019 +0100
superbuild_set_revision(nlohmannjson
  URL     "https://www.computationalmodelbuilder.org/files/dependencies/json-295732a81780378c62d1c095078b4634dac8ec28.tar.bz2"
  URL_MD5 2113211f84a0b01c1c70900285c81e2c)

superbuild_set_revision(meshkit
  SOURCE_DIR "${CMAKE_CURRENT_LIST_DIR}/meshkit")

superbuild_set_revision(rggsession
  GIT_REPOSITORY  "https://gitlab.kitware.com/cmb/plugins/rgg-session.git"
  GIT_TAG         origin/master
  GIT_SHALLOW     "${SUPERBUILD_SHALLOW_CLONES}")

superbuild_set_revision(opencascadesession
  GIT_REPOSITORY  "https://gitlab.kitware.com/cmb/plugins/opencascade-session.git"
  GIT_TAG         origin/master
  GIT_SHALLOW     "${SUPERBUILD_SHALLOW_CLONES}")

superbuild_set_revision(smtkresourcemanagerstate
  GIT_REPOSITORY "https://gitlab.kitware.com/cmb/plugins/read-and-write-resource-manager-state.git"
  GIT_TAG        "origin/master")

superbuild_set_revision(smtktruchasextensions
  GIT_REPOSITORY "https://gitlab.kitware.com/cmb/plugins/truchas-extensions.git"
  GIT_TAG        "origin/master")

superbuild_set_revision(smtkcmb2dextensions
  GIT_REPOSITORY "https://gitlab.kitware.com/cmb/plugins/cmb-2d.git"
  GIT_TAG        "origin/master")

superbuild_set_revision(pegtl
  # https://github.com/taocpp/PEGTL/releases/tag/2.7.1
  URL "https://www.computationalmodelbuilder.org/files/dependencies/PEGTL-2.7.1.tar.gz"
  URL_MD5 da0363623cf0936e4cb2648b001af257)

superbuild_set_revision(las
  URL     "https://www.paraview.org/files/dependencies/libLAS-1.8.1.tar.bz2"
  URL_MD5 2e6a975dafdf57f59a385ccb87eb5919)

if (APPLE)
  set(capstone_file "Capstone_MacOsX_V912_Git_37e7ecd9b.dmg")
  set(capstone_md5 d7d64e784864295282e487930b4aafee)
elseif (NOT WIN32)
  set(capstone_file "Capstone_Redhat_7_6_V912_Git_37e7ecd.tar.gz")
  set(capstone_md5 5ecfd45eaa7b9b8109e72aa6fcfc0844)
endif ()

superbuild_set_revision(capstone
  URL "file://${KW_SHARE}/Projects/CMB/dependencies/Capstone-9.1.2/${capstone_file}"
  URL_MD5 "${capstone_md5}")

if (APPLE)
  set(cubit_file "Cubit-15.2-Mac64.dmg")
  set(cubit_md5 2bf9dedc8029b32040f06414ba3b88f9)
elseif (NOT WIN32)
  set(cubit_file "Cubit-15.2-Lin64.tar")
  set(cubit_md5 4f2cb33ac8eae693fdc289bc4ef172f4)
endif ()

superbuild_set_revision(cubit
  URL "file://${KW_SHARE}/Projects/CMB/dependencies/Cubit-15.2/${cubit_file}"
  URL_MD5 "${cubit_md5}")

superbuild_set_selectable_source(aevasession
  SELECT git CUSTOMIZABLE DEFAULT
    GIT_REPOSITORY  "https://gitlab.kitware.com/aeva/session.git"
    GIT_TAG         "origin/master"
    GIT_SHALLOW     "${SUPERBUILD_SHALLOW_CLONES}"
  SELECT source CUSTOMIZABLE
    SOURCE_DIR "source-aevasession")

superbuild_set_selectable_source(aeva
  SELECT git CUSTOMIZABLE DEFAULT
    GIT_REPOSITORY  "https://gitlab.kitware.com/aeva/aeva.git"
    GIT_TAG         "origin/master"
    GIT_SHALLOW     "${SUPERBUILD_SHALLOW_CLONES}"
  SELECT source CUSTOMIZABLE
    SOURCE_DIR "source-aeva")

superbuild_set_revision(netgen
  URL     "https://www.computationalmodelbuilder.org/files/dependencies/netgen-6.2.2105.tar.gz"
  URL_MD5 63cbe3a720c955cc0e00055345db2ec2)

superbuild_set_selectable_source(xmscore
  SELECT superbuild DEFAULT
    URL "https://www.computationalmodelbuilder.org/files/dependencies/xmscore-superbuild-v1.1.tar.gz"
    URL_MD5 2124899e1e7016ffc9ba7f3852074644
  SELECT git CUSTOMIZABLE
    GIT_REPOSITORY "https://github.com/kwryankrattiger/xmscore.git"
    GIT_TAG kitware_superbuild
    GIT_SHALLOW "${SUPERBUILD_SHALLOW_CLONES}")

superbuild_set_selectable_source(xmsgrid
  SELECT superbuild DEFAULT
    URL "https://www.computationalmodelbuilder.org/files/dependencies/xmsgrid-superbuild-v1.1.tar.gz"
    URL_MD5 74488215f885ebc7b8ba0cf646f43758
  SELECT git CUSTOMIZABLE
    GIT_REPOSITORY "https://github.com/kwryankrattiger/xmsgrid.git"
    GIT_TAG kitware_superbuild
    GIT_SHALLOW "${SUPERBUILD_SHALLOW_CLONES}")

superbuild_set_selectable_source(xmsinterp
  SELECT superbuild DEFAULT
    URL "https://www.computationalmodelbuilder.org/files/dependencies/xmsinterp-superbuild-v1.1.tar.gz"
    URL_MD5 00cd91709b1bc7a0cc04e8457aff8f8b
  SELECT git CUSTOMIZABLE
    GIT_REPOSITORY "https://github.com/kwryankrattiger/xmsinterp.git"
    GIT_TAG kitware_superbuild
    GIT_SHALLOW "${SUPERBUILD_SHALLOW_CLONES}")

superbuild_set_selectable_source(xmsmesher
  SELECT superbuild DEFAULT
    URL "https://www.computationalmodelbuilder.org/files/dependencies/xmsmesher-superbuild-v1.1.tar.gz"
    URL_MD5 4262c439be048edfce28de62ce634f9b
  SELECT git CUSTOMIZABLE
    GIT_REPOSITORY "https://github.com/kwryankrattiger/xmsmesher.git"
    GIT_TAG kitware_superbuild
    GIT_SHALLOW "${SUPERBUILD_SHALLOW_CLONES}")

superbuild_set_revision(smtkxmsmesher
  GIT_REPOSITORY "https://gitlab.kitware.com/cmb/plugins/xmsmeshoperation.git"
  GIT_TAG "origin/master"
  GIT_SHALLOW "${SUPERBUILD_SHALLOW_CLONES}")
