superbuild_add_project(netgen
  DEPENDS zlib
  # DEPENDS_OPTIONAL occt python3
  CMAKE_ARGS
    -Wno-dev
    -DUSE_SUPERBUILD:BOOL=OFF
    -DUSE_OCC:BOOL=OFF #${occt_enabled}
    -DOCC_INCLUDE_DIR:PATH=<INSTALL_DIR>/include/opencascade
    -DOCC_LIBRARY:PATH=<INSTALL_DIR>/lib
    -DUSE_PYTHON:BOOL=OFF #${python3_enabled}
    -DUSE_INTERNAL_TCL:BOOL=OFF
    -DUSE_GUI:BOOL=OFF
    -DUSE_MPI4PY:BOOL=OFF
    -DUSE_NATIVE_ARCH:BOOL=OFF
    -DCMAKE_INSTALL_PREFIX=<INSTALL_DIR>
    -DNETGEN_NO_DEB_PACKAGER=ON
    # The install directories below are required on
    # at least some platforms. (Netgen will otherwise
    # install to /Application/Netgen.app on macos.)
    -DNG_INSTALL_DIR_BIN:STRING=bin # for windows DLLs
    -DNG_INSTALL_DIR_LIB:STRING=lib
    -DNG_INSTALL_DIR_CMAKE:STRING=lib/cmake/netgen
    # -DNG_INSTALL_DIR_PYTHON:STRING=lib/python3.7 # TODO: version number from superbuild?
    -DNG_INSTALL_DIR_RES:STRING=share
    -DNG_INSTALL_DIR_INCLUDE:STRING=include
)

# https://github.com/NGSolve/netgen/pull/70
superbuild_apply_patch(netgen disable-debian-assumptions
  "Disable assumptions about Linux platforms")

# Netgen just assumes it is either being built from a git checkout or Ubuntu
# does the work for it.
superbuild_apply_patch(netgen version-file
  "Add a version file to the source tree")
