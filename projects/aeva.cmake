set(aeva_extra_optional_dependencies)
if (USE_NONFREE_COMPONENTS)
  list(APPEND aeva_extra_optional_dependencies
    triangle)
endif ()

set(aeva_test_plugin_dir lib)
if (WIN32)
  set(aeva_test_plugin_dir bin)
endif()

set(aeva_extra_cmake_args)
if (UNIX AND NOT APPLE)
  list(APPEND aeva_extra_cmake_args
    -DCMAKE_INSTALL_RPATH_USE_LINK_PATH:BOOL=FALSE)
endif ()

get_property(aeva_lfs_steps GLOBAL
  PROPERTY cmb_superbuild_lfs_steps)

set(aeva_rpaths)
if (APPLE AND USE_SYSTEM_qt5)
  # On macOS, Qt5 packages use `@rpath` as their library ids. Add an rpath for
  # it to the build.
  list(APPEND aeva_rpaths
    "${qt5_rpath}")
endif ()
string(REPLACE ";" "${_superbuild_list_separator}"
  aeva_rpaths
  "${aeva_rpaths}")

set(aeva_plugins
  aeva-session)
if (opencascadesession_enabled)
  list(APPEND aeva_plugins
    opencascade-session)
endif ()
if (smtkresourcemanagerstate_enabled)
  list(APPEND aeva_plugins
    read-and-write-resource-manager-state)
endif ()
string(REPLACE ";" "${_superbuild_list_separator}"
  aeva_plugins
  "${aeva_plugins}")

if (POLICY CMP0114)
  set(aeva_step_keyword STEP_TARGETS)
else ()
  set(aeva_step_keyword INDEPENDENT_STEP_TARGETS)
endif ()

superbuild_add_project(aeva
  DEVELOPER_MODE
  DEBUGGABLE
  ${aeva_step_keyword} ${aeva_lfs_steps} download update
  DEPENDS aevasession boost moab nlohmannjson occt python3 paraview
          pybind11 qt5 smtk opencascadesession aevaexampledata
  DEPENDS_OPTIONAL ${aeva_extra_optional_dependencies}
                   cxx11 libarchive hdf5 netcdf opencv
                   smtkresourcemanagerstate
                   vxl
                   cmbusersguide smtkusersguide
  CMAKE_ARGS
    ${aeva_extra_cmake_args}
    -DBUILD_TESTING:BOOL=${BUILD_TESTING}
    -Daeva_enable_testing:BOOL=${TEST_aeva}

    -DCMB_EXTRA_SMTK_PLUGINS:STRING=${aeva_plugins}

    -DKML_DIR:PATH=<INSTALL_DIR>

    #specify semi-colon separated paths for session plugins
    -Daeva_test_plugin_paths:STRING=<INSTALL_DIR>/${aeva_test_plugin_dir}

    # specify the apple app install prefix. No harm in specifying it for all
    # platforms.
    -DMACOSX_APP_INSTALL_PREFIX:PATH=<INSTALL_DIR>/Applications

    # Set CMAKE_INSTALL_LIBDIR to "lib" for all projects in the superbuild to
    # override OS-specific libdirs that GNUInstallDirs.cmake would otherwise
    # set.
    -DCMAKE_INSTALL_LIBDIR:STRING=lib
    -DCMAKE_INSTALL_RPATH:STRING=${aeva_rpaths})

if ((CMAKE_CXX_COMPILER_ID STREQUAL "AppleClang" AND NOT CMAKE_CXX_COMPILER_VERSION VERSION_LESS "7.0") OR
    (CMAKE_CXX_COMPILER_ID STREQUAL "Clang" AND NOT CMAKE_CXX_COMPILER_VERSION VERSION_LESS "3.5"))
 superbuild_append_flags(cxx_flags "-Wno-inconsistent-missing-override" PROJECT_ONLY)
endif ()

superbuild_declare_paraview_xml_files(aeva
  FILE_NAMES "aevaCMB-postprocess.xml"
  DIRECTORY_HINTS "aevaCMB-2.4.0"
# Omit static plugins
  OMIT_PLUGINS cmbPostProcessingModePlugin
)

set(aeva_can_lfs FALSE)
if (aeva_SOURCE_SELECTION STREQUAL "git")
  set(aeva_can_lfs TRUE)
elseif (aeva_SOURCE_SELECTION STREQUAL "source")
  if (EXISTS "${aeva_SOURCE_DIR}/.git")
    set(aeva_can_lfs TRUE)
  endif ()
endif ()

option(aeva_FETCH_LFS "Fetch LFS data for AEVA" OFF)
if (aeva_enabled AND aeva_can_lfs AND aeva_FETCH_LFS)
  cmb_superbuild_add_lfs_steps(aeva)
endif ()
