set(VTKONLY_EXTRA_CMAKE_ARGUMENTS ""
  CACHE STRING "Extra arguments to be passed to vtk when configuring.")
mark_as_advanced(VTKONLY_EXTRA_CMAKE_ARGUMENTS)

set(vtk_extra_cmake_options)

if (UNIX AND NOT APPLE)
  list(APPEND vtk_extra_cmake_options
    -DCMAKE_INSTALL_RPATH_USE_LINK_PATH:BOOL=FALSE)
endif ()

set(vtkonly_qt_req NO)
if (qt5_enabled)
  set(vtkonly_qt_req YES)
endif ()

set(vtkonly_libarchive_req NO)
if (libarchive_enabled)
  set(vtkonly_libarchive_req YES)
endif ()

set(vtkonly_python_req NO)
if (python3_enabled)
  set(vtkonly_python_req YES)
endif ()

set(vtkonly_las_req NO)
if (las_enabled)
  set(vtkonly_las_req YES)
endif ()

set(vtk_smp_backend "Sequential")
if (tbb_enabled)
  set(vtk_smp_backend "TBB")
endif ()

set(vtk_dll_paths)
if (qt5_dllpath)
  list(APPEND vtk_dll_paths
    "${qt5_dllpath}")
endif ()
string(REPLACE ";" "${_superbuild_list_separator}" vtk_dll_paths "${vtk_dll_paths}")

if (APPLE)
  set(vtkonly_rpath "@loader_path/")
elseif (UNIX)
  set(vtkonly_rpath "${superbuild_install_location}/lib")
endif ()

superbuild_add_project(vtkonly
  DEBUGGABLE
  DEPENDS
    gdal
    png
    zlib
    netcdf
  DEPENDS_OPTIONAL
    cxx11 freetype libarchive python3 qt5 hdf5 las tbb
  CMAKE_ARGS
    -DBUILD_SHARED_LIBS:BOOL=ON
    -DVTK_BUILD_TESTING:BOOL=OFF
    -DVTK_WRAP_PYTHON:BOOL=${python3_enabled}
    -DVTK_PYTHON_VERSION:STRING=3
    -DVTK_ENABLE_KITS:BOOL=ON

    -DVTK_UNIFIED_INSTALL_TREE:BOOL=ON
    -DVTK_DLL_PATHS:STRING=${vtk_dll_paths}

    -DVTK_MODULE_ENABLE_VTK_GUISupportQt:STRING=${vtkonly_qt_req}
    # CMB needs geovis enabled to provide the gdal reader
    -DVTK_MODULE_ENABLE_VTK_CommonArchive:STRING=${vtkonly_libarchive_req}
    -DVTK_MODULE_ENABLE_VTK_GeovisCore:STRING=YES
    -DVTK_MODULE_ENABLE_VTK_IOGDAL:STRING=YES
    -DVTK_MODULE_ENABLE_VTK_IOLAS:STRING=${vtkonly_las_req}
    -DVTK_MODULE_ENABLE_VTK_IOParallelExodus:STRING=YES
    -DVTK_MODULE_ENABLE_VTK_RenderingContextOpenGL2:STRING=YES
    -DVTK_MODULE_ENABLE_VTK_RenderingGL2PSOpenGL2:STRING=YES
    -DVTK_MODULE_ENABLE_VTK_RenderingMatplotlib:STRING=${vtkonly_python_req}
    -DVTK_MODULE_ENABLE_VTK_RenderingQt:STRING=${vtkonly_qt_req}
    -DVTK_MODULE_ENABLE_VTK_TestingRendering:STRING=YES
    -DVTK_MODULE_ENABLE_VTK_ViewsInfovis:STRING=YES

    # since VTK mangles all the following, I wonder if there's any point in
    # making it use system versions.
    -DVTK_MODULE_USE_EXTERNAL_VTK_freetype:BOOL=${freetype_enabled}
    -DVTK_MODULE_USE_EXTERNAL_VTK_hdf5:BOOL=${hdf5_enabled}
    -DVTK_MODULE_USE_EXTERNAL_VTK_netcdf:BOOL=${netcdf_enabled}
    -DVTK_MODULE_USE_EXTERNAL_VTK_png:BOOL=${png_enabled}
    -DVTK_MODULE_USE_EXTERNAL_VTK_zlib:BOOL=${zlib_enabled}

    -DVTK_GROUP_ENABLE_Web:STRING=${vtkonly_python_req}
    -DVTK_SMP_IMPLEMENTATION_TYPE:STRING=${vtk_smp_backend}

    -DCMAKE_INSTALL_LIBDIR:STRING=lib
    -DCMAKE_INSTALL_RPATH:STRING=${vtkonly_rpath}
    -DCMAKE_INSTALL_NAME_DIR:STRING=<INSTALL_DIR>/lib

    ${vtk_extra_cmake_options}
    ${VTKONLY_EXTRA_CMAKE_ARGUMENTS})

if (vtkonly_SOURCE_SELECTION STREQUAL "release")
  # XXX(vtk-9.0)
  superbuild_apply_patch(vtkonly missing-includes
    "Fix missing includes with newer compilers")
endif ()

superbuild_add_extra_cmake_args(
  -DVTK_DIR:PATH=<INSTALL_DIR>/lib/cmake/vtk-${vtk_version})
